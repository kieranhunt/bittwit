#!/usr/bin/python

from btsync import BTSync
from pprint import pprint
from Tkinter import *
import datetime
import json
import os
import sqlite3 as lite
import sys
import time

class MainWindow:

    def __init__(self, master):
        """
        Constructor for the main window.
        Defines the location and functions each of the items in the main window.

        Keyword arguments:
        master -- The window from which this window spawns. Its parent.
        """

        self.messages = []

		#Top Buttons

        self.followButton = Button(master, text="Follow", command=self.newFollow)
        self.followButton.grid(row=0, column=0)
        
        self.followButton = Button(master, text="Refresh", command=self.refresh)
        self.followButton.grid(row=0, column=1)
        
        self.followButton = Button(master, text="Unfollow", command=self.unfollow)
        self.followButton.grid(row=0, column=2)
        
        #Message Panel

        self.frame = Frame(master, width=600, height=480, bg="grey")
        self.frame.grid(row=1, column=0, columnspan=3)
        
        #Bottom Buttons
        
        self.followButton = Button(master, text="Followers", command=self.ShowFollowerList)
        self.followButton.grid(row=2, column=0)

        self.followButton = Button(master, text="Post", command=self.showNewPost)
        self.followButton.grid(row=2, column=1)
        
        self.followButton = Button(master, text="Following", command=self.showFollowingList)
        self.followButton.grid(row=2, column=2)
        
        
        #Bottom Info Panel
        
        with con:
            cur = con.cursor()
            cur.execute('SELECT SQLITE_VERSION()')
            data = cur.fetchone()
        
            self.SQLiteVersionLabel = Label(master, text="SQLite DB version: %s" % data, anchor="w")
            self.SQLiteVersionLabel.grid(row=3, column=0)

            speed = btsync.get_speed()["download"]

            self.OSVersionLabel = Label(master, text="Speed: %s" % speed)
            self.OSVersionLabel.grid(row=3, column=2)
					
        self.refresh()
        
    def refresh(self):
        """
        Refreshes the posts displayed within the main window.
        Searches through all of the directories and finds all of the files
        with .txt extensions. Searches through both personal and other users'
        directories.

        Keyword arguments:
        None
        """
        self.messages = []
        for path, subdirs, files in os.walk(r'data'):
            for subdir in subdirs:
                for subpath, subdirs, subfiles in os.walk(r'data/'+subdir):
                    for filename in subfiles:                        
                        if (filename.endswith('.txt')):                       
                            self.messages.append(open(subpath+'/'+filename, 'r').read())
        print self.messages

        for index, message in enumerate(self.messages):
            self.messages[index] = [message, Label(self.frame, text=message)]
            self.messages[index][1].grid(row=index)
                

    def newFollow(self):
        """
        Spawns a new follow window and names it.

        Keyword arguments:
        None
        """
        addNewFollowWindow = Toplevel(root)
        addNewFollowWindow.wm_title('Follow someone')

        followWindow = FollowWindow(addNewFollowWindow)

    def unfollow(self):
        """
        Spawns an unfollow window and names it.

        Keyword arguments:
        None
        """
        removeFollowWindow = Toplevel(root)
        removeFollowWindow.wm_title('Remove someone')

        removeWindow = RemoveWindow(removeFollowWindow)
		
    def ShowFollowerList(self):
        """
        Spawns the follower list window and names it.

        Keyword arguments:
        None
        """
        addshowFollowerList = Toplevel(root)
        addshowFollowerList.wm_title('')

        showFollowerListWindow = ShowFollowerList(addshowFollowerList)
      
    def showFollowingList(self):
        """
        Spawns the following list window and names it.

        Keyword arguments:
        None
        """
        print "click!"

    def showNewPost(self):
        """
        Spawns a new post window and names it.

        Keyword arguments:
        None
        """
        addshowNewPost = Toplevel(root)
        addshowNewPost.wm_title('')

        showNewPostWindow = ShowNewPostWindow(addshowNewPost)

class FollowWindow:

    def __init__(self, master):
        """
        The constructor for the follow window. Places all of the items on the GUI.

        Keyword arguments:
        master -- The parent GUI from which the window will be spawned.
        """
        self.secretFieldLabel = Label(master, text='secret')
        self.secretFieldLabel.grid(row=0, column=0)
        self.secretField = Entry(master)
        self.secretField.grid(row=0, column=1, columnspan=2)

        self.aliasField = Entry(master)
        self.aliasField.grid(row=1, column=1, columnspan=2)
        self.aliasFieldLabel = Label(master, text='alias (optional)')
        self.aliasFieldLabel.grid(row=1, column=0)

        self.followButton = Button(master, text="Follow", command=self.followNew)
        self.followButton.grid(row=2, column=2)

    def followNew(self):
        """
        Handles adding new subscriptions.
        Checks to see if the subscription already exists.
        If not it creates the folder,
        adds the secret to BitTorrent Sync and
        inserts the record into the database.

        Keyword arguments:
        None
        """  
        self.secret = self.secretField.get()
        self.alias = self.aliasField.get()
  
        cur = con.cursor()
        cur.execute('SELECT count(*) from following where ID is \"' + self.secret + '\";')
        numAlready  = cur.fetchone()
        print numAlready

        if (numAlready[0] != 0):
            print "User already followed"
            return
        
        newFolderPath = os.getcwd() + r'/data/yours/' + self.secret
        print newFolderPath
        if not os.path.exists(newFolderPath): os.makedirs(newFolderPath) 
	
        outcome = btsync.add_folder(newFolderPath, self.secret)
        
        if (outcome["error"] != 0):
            print "BTSync Error: ", outcome["error"]
            return
        
        cur = con.cursor()
        query = 'INSERT INTO following values (\"' + self.secret + '\", \"' + self.alias + '\");';
        print query
        cur.execute(query) #not working, might need a commit transaction.
        con.commit()

class RemoveWindow:

    def __init__(self, master):
        """
        The constructor the for remove window.
        places each of the followers on the window and adds
        buttons to each one.

        Keyword arguments:
        master -- The parent GUI from which this one spawns.
        """
        cur = con.cursor()
        cur.execute('SELECT * from following;')
        rows = cur.fetchall()
        self.unFollowButtons = []

        for i, row in enumerate(rows):
            self.IDLabel = Label(master, text=row[0])
            self.IDLabel.grid(row=i, column=0)
        
            self.AliasLabel = Label(master, text=row[1])
            self.AliasLabel.grid(row=i, column=1)

            self.unFollowButtons.append(Button(master, text="Unfollow", command=self.removeFollow(row[0])))
            self.unFollowButtons[i].grid(row=i, column=2)

    def removeFollow(self, ID):
        """
        Removes a follower.

        Keyword arguments:
        ID -- The identification secret of the subscription to be removed.
        """
        print "test"

class ShowFollowerList:

    def __init__(self, master):
        """
        The constructor to create the follower list window.

        Keyword arguments:
        master -- the GUI from which the window spawns.        
        """
        self.followersjson = btsync.get_folder_peers(mySecret)

        for i, follower in enumerate(self.followersjson):
            self.IDLabel = Label(master, text=" - " + self.followersjson[i]["name"])
            self.IDLabel.grid(row=i, column=0)

class ShowNewPostWindow:

    def __init__(self, master):
        """
        Constructor for the new post window. Creates the GUI items.

        Keyword arguments:
        master -- the GUI from which this window spawns. Its parent window.
        """
        self.postField = Entry(master)
        self.postField.grid(row=0, column=0, columnspan=2)

        self.makePostButton = Button(master, text="Post", command = self.newPost)
        self.makePostButton.grid(row=1, column=1)

    def newPost(self):
        """
        Creates a new post by writing the content to a text file.
        Keyword arguments:
        None.
        """
        self.newfileName = os.getcwd() + r'/data/mine/' + str(int(time.time())) + ".txt"
        self.newfile = open(self.newfileName, 'w')
        self.newfile.write(self.postField.get())
        self.newfile.close()
        



con = lite.connect('BitTwit.db')

mySecret = "ABLOVO2REHZYE2OWNTPXZKVJQVVYBRSRT"

btsync = BTSync()

root = Tk()
root.wm_title("BitTwit")

app = MainWindow(root)

root.mainloop()
root.destroy() # optional; see description below
